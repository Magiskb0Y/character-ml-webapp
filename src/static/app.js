const options = {
    color: '000',
    size: 20,
    controls: [
        { Size: {type: 'range'} },
        { Navigation: { back: false, forward: false } }
    ]
}

const board = new DrawingBoard.Board('board');

DrawingBoard.Control.Predict = DrawingBoard.Control.extend({

	name: 'predict',

	initialize: function() {
		this.$el.append('<button class="drawing-board-control-download-button">Predict</button>');
		this.$el.on('click', '.drawing-board-control-download-button', $.proxy(function(e) {
            const payload = {
                image: board.canvas.toDataURL('image/png')
            }
            const url = window.location.origin + '/predict';
            
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify(payload),
                success: function(data, textStatus, xhr) {
                    $('#label').html(data.label);
                },
                error: function(xhr, status, err) {
                    console.log(err);
                }
            })
		}, this));
	}

});

const downloadControl = new DrawingBoard.Control.Predict(board).addToBoard();