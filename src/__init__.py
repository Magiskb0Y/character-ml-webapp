import os

from flask import Flask
from flask import request
from flask import jsonify
from flask import render_template
from flask_cors import CORS

from . import helper

app = Flask(__name__)
model = helper.load_model()

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/predict', methods=['POST'])
def predict():
    global model

    try:
        base64 = request.get_json()['image']
    except KeyError as err:
        return jsonify(error='Image not found')

    X = helper.base64cvtndarray(base64)
    result = helper.predict(model, [X])

    return jsonify(label=int(result))