import os
import io
import base64

import numpy as np
from PIL import Image
import tensorflow as tf


base = os.path.abspath(os.path.dirname(__file__))

def load_model():
    sess = tf.Session()
    tf.saved_model.loader.load(
        sess,
        [tf.saved_model.tag_constants.TRAINING],
        os.path.join(base, 'model')
    )

    graph = tf.get_default_graph()

    features = graph.get_tensor_by_name('dataset/features:0')
    predictor = graph.get_tensor_by_name('real_output:0')

    return {'input': features, 'output': predictor, 'session': sess}


def predict(model, X):
    p = model['session'].run(model['output'], feed_dict={model['input']: X})
    return p[0]


def base64cvtndarray(base64_string):
    data = base64_string.split(',')[1]
    img = Image.open(io.BytesIO(base64.b64decode(data))).convert('L').resize((28, 28))
    src = np.array(img)
    src = np.invert(src) / 255.0
    return src.reshape(-1)